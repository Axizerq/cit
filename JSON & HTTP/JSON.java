package com.journaldev.jackson.model;
//package edu.tuckettt.json;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.tuckettt.json.HTTP;

public class JSON {

    public static String getHttpConnect(ObjectMapper string) {

        String r = "";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            r = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        } catch (Throwable cause) {
            cause.printStackTrace();
        }
        return r;
    }


   /* public static String httpToJSON(HTTP http) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        try {
            s = mapper.writeValueAsString(http);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    } */

    //JSON into object
    public static HTTP JSONToHttp(String req) {

        ObjectMapper mapper = new ObjectMapper();
        HTTP http = null;

        try {
            http = mapper.readValue(req, HTTP.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return http;
    }

    //Display Objects
    public static void main(String[] args) {

       HTTP http1 = new HTTP();
       String json = HTTP.httpToJSON(http1);

       HTTP http1 = JSON.JSONToHttp(json);
       System.out.println(http1);
    }

}
