package HT;

import HT.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.util.*;

public class StudentDAO {

    SessionFactory factory = null;
    Session session = null;

    private static StudentDAO single_instance = null;

    public Student findById(int id) {
        return Utils.getSessionFactory().openSession().get(Student.class, id);
    }

    private StudentDAO()
    {
        factory = Utils.getSessionFactory();
    }

    public static StudentDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new StudentDAO();
        }

        return single_instance;
    }

    public List<Student> getStudents() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.Student";
            List<Student> cs = (List<Student>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /* public Student getStudent(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "Id is " + Integer.toString(id);
            Student c = (Student)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    } */

}