import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.junit.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import static junit.framework.Assert.*;

public class CalculateTest {

    @After
    public void After() {
        System.out.println("This is @After every test");
    }

    @Test
    public void test1() throws Exception {

        Calculate calculate = new Calculate();
        int x = calculate.Sum(1, 2);

        assertEquals(3, x);
    }

    @Test
    public void test2() {

        Calculate calculate = new Calculate();
        int x = calculate.Sum(11, -3);
        int y = calculate.Sum(11, 3);

        assertNotSame(x, y);
    }

    @Test
    public void test3() {

        Calculate calculate = new Calculate();
        int x = calculate.Sum(11, -3);

        assertNotNull(x);
    }

    @BeforeClass
    public static void Before() {
        System.out.println("This is @BeforeClass");
    }
}